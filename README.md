we will do a test run of a pipeline for CI:

feature branch:

git-->bitbucket-->[jenkins]*--->[artifactory]

- this will be a simple java app
- anything inside a [] is a container
- jenkins will be used for simple JUnit test verification prior to pushing package to artifactory
- i want jenkins to build the container as well and push that to artifactory (acting as a docker repository)
- initially i'll deploy these containers on my laptop, and in the future, on AWS.

future prototyping

I want to be able to use an integrated branch commit, to trigger a CD:
meaning, 
git-->bitbucket-->[jenkins]<---->[artifactory]---->AWS elastic container  [intermediate]

final:
git-->bitbucket-->[jenkins]<---->[artifactory]---->cloud foundry running on AWS.





integration branch:
git-->bitbucket-->[jenkins]**--->[sonarQube]--->cicd-01 registry

CD 
git-->bitbucket-->[jenkins]--->[PivotalCloudFoundry]***

* jenkins runs unit tests , and upon passing sends to this dockerhub registry
** jenkins runs integration tests, and upon passing integration tests , sends through sonarQube for code check, and hosts it eventually in dockerhub
***[PivotalCloudFoundry] images need to be baked and created locally and pushed to this docker-hub registry as well.

jenkins, sonarQube and PCF are all running in AWS. 
